/**
 * ProcessError
 * Process a Node.Js error to return response information.
 *
 * @param {Error} err - The error
 * @return {object} - The response data
 */
const processError = (err) => {
    const code = err.code || err.name;
    const response = {
        success: false,
        error: {
            code: 'UnknownError',
            message: err.message || 'Unknown error',
        },
    };

    if (code) {
        switch (code) {
        case 'ValidationError':
            response.error.code = 'ValidationError';
            response.error.message = err.message;
            if (err.data && err.data.details) {
                const fields = [];
                err.data.details.forEach((detail) => {
                    fields.push(detail.message);
                });
                response.error.fields = fields;
            }
            break;
        case 'Unauthorized':
        case 'BadRequest':
            response.error.code = code;
            response.error.message = err.message;
            break;
        default:
            break;
        }
    }

    return response;
};

/**
 * ProcessResponse
 * Processes a standard response with arbitrary data.
 *
 * @param {mixed} data - The response data (should probably be an object with key/value pairs
 * @return {object} - The response object.
 */
const processResponse = (data) => {
    const response = {
        data,
        success: true,
    };

    return response;
};

/**
 * ProcessItemResponse
 * Process a response and generate the content
 *
 * @param {string} itemType - The type of item we are returning.
 * @param {object} item - The object we are returning.
 * @return {object} - The response object.
 */
const processItemResponse = (itemType, item) => processResponse({ item, type: itemType });

// Export functions
exports.processError = processError;
exports.processItemResponse = processItemResponse;
exports.processResponse = processResponse;

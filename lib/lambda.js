let AWS;
let lambdaConfig = {};

/**
 * SetConfig
 * Set the lambda configuration settings.
 *
 * @param {object} config - The lambda config
 */
const setConfig = (config) => {
    lambdaConfig = config;
};

/**
 * SetSDKInstance
 * Set the AWS SDK Instance
 *
 * @param {AWS} AwsSdk - The AWS SDK to set globally.
 */
const setSDKInstance = (AwsSdk) => {
    AWS = AwsSdk;
};

/**
 * InvokeFunction
 * Invoke a lambda function
 *
 * @param {string} functionName - The name of the function to call
 * @param {object} payload - The payload to send.
 * @param {bool} asyncRequest - (optional) Invoke the function asyncronously
 * @return {Promise} - Return a promise.
 */
const invokeFunction = (functionName, payload, asyncRequest) => {
    const Lambda = new AWS.Lambda(lambdaConfig);

    const params = {
        FunctionName: functionName,
        InvocationType: asyncRequest ? 'Event' : 'RequestResponse',
        Payload: JSON.stringify(payload),
    };
    return Lambda.invoke(params).promise();
};

/**
 * Invoke
 * Invoke a lambda function
 *
 * @param {string} functionName - The name of the function to call
 * @param {object} payload - The payload to send.
 * @return {Promise} - Return a promise.
 */
const invoke = (functionName, payload) => invokeFunction(functionName, payload, false);

/**
 * InvokeAsync
 * Invoke a lambda function asyncronously
 *
 * @param {string} functionName - The name of the function to call
 * @param {object} payload - The payload to send.
 * @return {Promise} - Return a promise.
 */
const invokeAsync = (functionName, payload) => invokeFunction(functionName, payload, true);

// Export functions
exports.invokeAsync = invokeAsync;
exports.invoke = invoke;
exports.setConfig = setConfig;
exports.setSDKInstance = setSDKInstance;

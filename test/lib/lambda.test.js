const AWSMock = require('aws-sdk-mock');
const AWS = require('aws-sdk');
const { assert } = require('chai');
const Lambda = require('../../lib/lambda.js');

/**
 * GenerateMockResponse
 * Generate a mock response from the stakehex-user-service
 *
 * @param {string} status - The status of the response
 * @return {string} - JSON stringified response object.
 */
const generateMockResponse = () => ({
    StatusCode: 200,
    Payload: JSON.stringify({
        success: true,
        data: {
            item1: 'value1',
        },
    }),
});

before(() => {
    AWSMock.setSDKInstance(AWS);
});
afterEach(() => {
    AWSMock.restore();
});

describe('Lambda.setConfig()', () => {
    Lambda.setSDKInstance(AWS);

    it('should exist and take a single argument', () => {
        assert.isDefined(Lambda.setConfig);
        assert.isFunction(Lambda.setConfig);
    });
});

describe('Lambda.invoke()', () => {
    Lambda.setSDKInstance(AWS);

    it('should invoke a lambda function', async () => {
        AWSMock.mock('Lambda', 'invoke', generateMockResponse());

        const response = await Lambda.invoke('lambda-function', { data: 'test' });
        const payload = JSON.parse(response.Payload);

        assert.equal(response.StatusCode, 200);
        assert.isTrue(payload.success);
        assert.equal(payload.data.item1, 'value1');
    });
});

describe('Lambda.invokeAsync()', () => {
    Lambda.setSDKInstance(AWS);

    it('should invoke a lambda function', async () => {
        AWSMock.mock('Lambda', 'invoke', generateMockResponse());

        const response = await Lambda.invoke('lambda-function', { data: 'test' });
        const payload = JSON.parse(response.Payload);

        assert.equal(response.StatusCode, 200);
        assert.isTrue(payload.success);
        assert.equal(payload.data.item1, 'value1');
    });
});

const { assert } = require('chai');

const { processError, processItemResponse, processResponse } = require('../../lib/response-handler.js');

describe('response-handler.processError()', () => {
    it('should default to UnknownError', () => {
        const err = new Error();
        const expected = {
            success: false,
            error: {
                code: 'UnknownError',
                message: 'Unknown error',
            },
        };

        assert.deepEqual(processError(err), expected);
    });

    it('Should include a custom message if defined for the error', () => {
        const err = new Error('Custom message');
        const expected = {
            success: false,
            error: {
                code: 'UnknownError',
                message: 'Custom message',
            },
        };

        assert.deepEqual(processError(err), expected);
    });

    it('Should change status code and code on ValidationError', () => {
        const err = new Error('Custom message');
        err.code = 'ValidationError';

        const expected = {
            success: false,
            error: {
                code: 'ValidationError',
                message: 'Custom message',
            },
        };

        assert.deepEqual(processError(err), expected);
    });
});

describe('response-handler.processItemResponse()', () => {
    it('should return a successful response', () => {
        const itemType = 'U-UserDetail';
        const item = {
            message: 'First data',
            number: 45,
        };

        const expected = {
            success: true,
            data: {
                type: itemType,
                item,
            },
        };

        assert.deepEqual(processItemResponse('U-UserDetail', item), expected);
    });
});

describe('response-handler.processResponse()', () => {
    it('should return a successful response', () => {
        const data = {
            key: 'Value 1',
            boolean: false,
            integer: 5,
            nested: {
                under: 'One',
            },
        };

        const expected = {
            success: true,
            data: {
                key: 'Value 1',
                boolean: false,
                integer: 5,
                nested: {
                    under: 'One',
                },
            },
        };

        assert.deepEqual(processResponse(data), expected);
    });
});

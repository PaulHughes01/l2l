const { assert } = require('chai');

const { Lambda, ResponseHandler } = require('../index.js');

describe('index.Lambda', () => {
    it('should return the Lambda library', () => {
        const {
            invokeAsync,
            invoke,
            setConfig,
            setSDKInstance,
        } = Lambda;

        assert.isFunction(invokeAsync);
        assert.isFunction(invoke);
        assert.isFunction(setConfig);
        assert.isFunction(setSDKInstance);
    });
});

describe('index.ResponseHandler', () => {
    it('should return the ResponseHandler', () => {
        const { processError, processItemResponse, processResponse } = ResponseHandler;

        assert.isFunction(processError);
        assert.isFunction(processItemResponse);
        assert.isFunction(processResponse);
    });
});

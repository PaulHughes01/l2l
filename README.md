# l2l - Lambda-2-Lambda Request/Response

l2l is a Node.js library that makes invoking AWS  Lambda functions within other Lambda functions straightforward and structured.

## Guide

To get started, add l2l as a dependency in your repository

```bash
$ npm install --save bitbucket:PaulHughes01/l2l.git
```

Then, depending on if your function is invoking other lambda functions, or responding to other lambda functions, you'll want to focus on either the `RequestHandler` or the `Lambda ` section of the library.


### Invoke external lambda functions

#### IAM Note

Make sure that the IAM execution role of your function has the `lambda:invoke` permission for the function it is invoking.

#### Integration
If you are invoking lambda functions, you can include that piece by adding it to the top of your lambda function code:

```js
const AWS = require('aws-sdk');
// ... other requires
const l2lLambda = require('l2l').Lambda;
const { invoke, invokeAsync } = l2lLambda;
```

Then, when you need to invoke the function in one of your handlers, you can call either of the invoke functions to do so.

Note that both `invoke` and `invokeAsync` are asynchronous methods; invoke simply maps to the AWS SDK Lambda function [invoke](https://docs.aws.amazon.com/AWSJavaScriptSDK/latest/AWS/Lambda.html#invoke-property) (the Promise resolves with the lambda function response data), and `invokeAsync` maps to the SDK's [invokeAsync](https://docs.aws.amazon.com/AWSJavaScriptSDK/latest/AWS/Lambda.html#invokeAsync-property) function, which resolves as soon as the invoked function receives the request (so the function data is not returned, and the actual execution could still fail).

You should use `invokeAsync` if speed is important and the invoked function's resolution isn't needed, and `invoke` if you need the result and response data of the invoked function after it completes (a more standard REST-style request). `invoke` is also what you may want to use if you are invoking a function that uses the ResponseHandler piece of this repository to structure its responses.

```js
exports.handler = async (event) => {
	let response;
	const functionName = 'other-lambda-function-name';
	const requestData = {
		message: 'Hello world!',
	};

	// This will return response data.
	const response = await invoke(functionName, requestData);
	
	// Or, invoke the function without waiting for the invoked function to complete
	await invokeAsync(functionName, requestData);

	// Return the response (if you used standard invoke)
	return response;
};
```

### Response Helper/Handler

The response handler (perhaps I should have chosen a better term) consists of a few functions that can be used to structure responses by invoked functions in a consistent way, which can help unify interfaces throughout a project.

First, include the library at the top of your lambda function:

```js
const { processError, processResponse } = require('l2l').ResponseHandler;
```

Then, in your handler, you can use those functions to return your responses to the invoking function in a clear way.

```js
exports.handler = async (event) => {
	let response;
	try {
		response = {
			message: 'Hello, world!'
		};
	} catch (err) {
		return processError(err);
	}

	return processResponse(response);
};
```

The response to the invoking function, then, would end up as:

```json
{
	"data": {
		"message": "Hello, world!"
	},
	"success": true
}
```

An error thrown would follow this format (with the content depending on the type of error):

```json
{
	"error": {
		"code": "UnknownError",
		"message": "Unknown error"
	},
	"success": false
}
```


### Custom AWS SDK

If you are using a custom aws-sdk (for example if you are using aws-mock-sdk locally, or aws-xray-sdk to wrap it for x-ray tracing in AWS) then you can specify the custom AWS sdk and config for your local testing environment as follows.

```js
l2lLambda.setSDKInstance(customAWS);
l2lLabmda.setConfig({
    endpoint: 'http://localhost:4000',
	accessKeyId: 'xxxx',
	secretAccessKey: 'xxxx',
	apiVersion: '2015-03-31'
});
```

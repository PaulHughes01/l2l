let AWS = require('aws-sdk');

/**
 * Lambda
 * The lambda library with the invoking functions.
 *
 * @return {object} Lambda - The internal lambda library.
 */
const Lambda = require('./lib/lambda.js');

/**
 * ResponseHandler
 * The response handler library.
 *
 * @return {object} ResponseHandler - The response handler library.
 */
const ResponseHandler = require('./lib/response-handler.js');

/**
 * SetSDKInstance
 * Set the AWS SDK Instance globally within the package
 *
 * @param {AWS} AwsSdk - The AWS SDK to set globally.
 */
const setSDKInstance = (AwsSdk) => {
    AWS = AwsSdk;
    Lambda.setSDKInstance(AwsSdk);
};

setSDKInstance(AWS);

// Export the libraries and functions
exports.Lambda = Lambda;
exports.ResponseHandler = ResponseHandler;
exports.setSDKInstance = setSDKInstance;
